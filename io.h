//
//  io.h
//  MMAI_whoisyourdaddy
//
//  Created by O_o on 2015/12/15.
//  Copyright (c) 2015年 O_o. All rights reserved.
//

#ifndef __MMAI_whoisyourdaddy__io__
#define __MMAI_whoisyourdaddy__io__

#include <stdio.h>
#include <string>
#include "config.h"
#include "json11.hpp"
#include <fstream>
using namespace std ;
//define一些常數
struct jsonIO{
    string fileName ;
    jsonIO(string _file_name):fileName(_file_name){}
    inline json11::Json readJson(){
        ifstream ifs(fileName);
        string err ;
        if(!ifs) throw ;
        string content((istreambuf_iterator<char>(ifs)),(istreambuf_iterator<char>()) );
        if(content.empty()) throw;
        json11::Json json = json11::Json::parse(content, err) ;
        ifs.close() ;
        return json ;
    }
    inline void writeJson(json11::Json json){
        ofstream ofs(fileName) ;
        if(!ofs) throw ;
        string content = json.dump() ;
        ofs << content ;
        ofs.close() ;
    }
    
    
    
};
#endif /* defined(__MMAI_whoisyourdaddy__io__) */

