//
//  database.h
//  MMAI_whoisyourdaddy
//
//  Created by O_o on 2015/12/17.
//  Copyright (c) 2015年 O_o. All rights reserved.
//

#ifndef __MMAI_whoisyourdaddy__database__
#define __MMAI_whoisyourdaddy__database__

#include <stdio.h>
#include "person.h"
#include "io.h"
#include "distance.h"
struct QueryReport{
    PERSON_ID name ;
    vector<float> percision ;
    vector<float> recall ;
    float averagePercision ;
    QueryReport(PERSON_ID n, vector<float> p, vector<float> r, float ap):name(n), percision(p),recall(r), averagePercision(ap) {}
    
};
ostream& operator<<(ostream& out, const QueryReport& report) ;
struct Database{
    map<PERSON_ID, Person> people ;
    map<PERSON_ID, vector<pair<PERSON_ID, DISTANCE>>> distanceBetween ;
    
    void load(string cache_file) ;
    void load(function<float(vector<float>, Matrix)> metric, string method = "") ;
    void loadMeta() ;
    void loadDistance(string cache_file) ;
    
    void initDistance(function<float(vector<float>, Matrix)> lambda, string method = "") ;
    
    vector<Result> query(PERSON_ID name) const;
    QueryReport queryReport(PERSON_ID name) const;
    vector<QueryReport> queryAllReport() const ;
    
};


#endif /* defined(__MMAI_whoisyourdaddy__database__) */
