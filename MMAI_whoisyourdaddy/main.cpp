//
//  main.cpp
//  MMAI_whoisyourdaddy
//
//  Created by O_o on 2015/12/15.
//  Copyright (c) 2015年 O_o. All rights reserved.
//
#include "../database.h"
#include "evaluation.h"

int main(int argc, const char * argv[]) {
    Database db  ;
    auto summary = [](vector<QueryReport> reports){
        ofstream ofs(SUMMARY);
        float map, mrank ;
        for_each(reports.begin(), reports.end(), [&ofs, &map, &mrank](QueryReport r){
            ofs<<r;
            mrank += r.percision.size() ;
            map += r.averagePercision ;
        }) ;
        ofs<<"MAP:"<<map/reports.size()<<"MRANK:"<<mrank/reports.size()<<endl ;
        
    };
    db.load([](vector<float> d, Matrix m)->float{
        float dis = 0.f ;
        for_each(d.begin(), d.end(), [&dis](float element){dis+=pow(element,2);}) ;
        return dis ;
    }, "l2") ;
    vector<QueryReport> reports = db.queryAllReport() ;
    summary(reports) ;
    return 0;
}
