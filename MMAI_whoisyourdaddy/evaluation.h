//
//  evaluation.h
//  MMAI_whoisyourdaddy
//
//  Created by O_o on 2015/12/15.
//  Copyright (c) 2015年 O_o. All rights reserved.
//

#ifndef __MMAI_whoisyourdaddy__evaluation__
#define __MMAI_whoisyourdaddy__evaluation__

#include <stdio.h>
#include "person.h"
#include "database.h"
#include <iostream>
using namespace std ;
#endif /* defined(__MMAI_whoisyourdaddy__evaluation__) */


struct QueryEvaluation{
    Database const &db ;
    PERSON_ID id ;
    vector<bool> correctness ;
    inline QueryEvaluation(Database const& db, PERSON_ID id, vector<Result> results):id(id),db(db){
        sort(results.begin(), results.end(), [](const Result &lhs, const Result &rhs)->bool{return rhs.distance<lhs.distance;}) ;
        for_each(results.begin(), results.end(), [this, db, id](Result r){correctness.push_back(checkEachResult(db,id,r));}) ;
        
    }
    vector<float> percision ;
    vector<float> recall ;
    float averagePercision;
    friend ostream& operator<<(ostream&, const QueryEvaluation&) ;
    
    inline bool checkEachResult(Database db, PERSON_ID id, Result r){
        return db.people[id].kinships.end() != find(db.people[id].kinships.begin(), db.people[id].kinships.end(), id) ;
    }
    
};




