import scipy.io
import sys
import os
import json
def parse_name_lists(r, names):
	mat = scipy.io.loadmat(r+"_name_lists.mat")
	for name in mat['namelists']:
		person  = {"name":name, "kinships":[], "feature":[]}
		person["kinships"].append(name.replace("_1", "_2")) if name.find("_1")!=-1 else person["kinships"].append(name.replace("_2", "_1"))
		names.append(person)

def parse_LBP(r, names):
	mat = scipy.io.loadmat(r+"_LBP.mat")
	for p, fea in zip(names, mat['fea']):
		p["feature"] = fea.tolist()


names = []
for r in ["fd", "fs", "md", "ms"]:
	names_n = []
	parse_name_lists(r, names_n)
	parse_LBP(r, names_n)
	names += names_n
	n = {"data":names}
	with open("metadata_"+r+".json", "w") as f:
		json.dump(n,f)
n = {"data":names}
with open("metadata.json", "w") as f:
	json.dump(n,f)
