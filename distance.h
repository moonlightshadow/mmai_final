//
//  distance.h
//  MMAI_whoisyourdaddy
//
//  Created by O_o on 2015/12/15.
//  Copyright (c) 2015年 O_o. All rights reserved.
//

#ifndef __MMAI_whoisyourdaddy__distance__
#define __MMAI_whoisyourdaddy__distance__

#include <stdio.h>
#include <vector>
#include <functional>
#include <cmath>
enum METRIC{L1, L2} ;
using namespace std ;

struct Matrix{
    
};
/*
namespace Norms{
    function<float(float, float)> l1 = [](float e1, float e2){return abs(e1-e2) ;} ;
    function<float(float, float)> l2 = [](float e1, float e2){return pow((e1-e2),2) ;} ;
    
}*/

struct Distance{
    Distance(vector<float> f1, vector<float> f2, Matrix m) : f1(f1),f2(f2) {
        if(f1.size()!=f2.size()) throw ;
        auto it1 = f1.begin(), it2 = f2.begin() ;
        for(;it1!=f1.end()&&it2!=f2.end(); it1++, it2++){
            difference.push_back(abs(*it1-*it2)) ;
        }
    }
    float operator()(function<float(vector<float>, Matrix)> lambda) const {return lambda(difference, m) ;}
    
    vector<float> f1, f2, difference ;
    Matrix m ;
};


#endif /* defined(__MMAI_whoisyourdaddy__distance__) */

