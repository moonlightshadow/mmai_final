//
//  database.cpp
//  MMAI_whoisyourdaddy
//
//  Created by O_o on 2015/12/17.
//  Copyright (c) 2015年 O_o. All rights reserved.
//

#include "database.h"
ofstream queryLogger(LOGGER) ;

void Database::load(string cache_file){
    loadMeta() ;
    loadDistance(cache_file) ;
}

void Database::load(function<float(vector<float>, Matrix)> metric, string method){
    loadMeta() ;
    initDistance(metric, method) ;
}

void Database::loadMeta(){
    jsonIO meta = jsonIO(META) ;
    json11::Json json = meta.readJson()["data"] ;
    auto parseItem = [this](json11::Json item){
        string name = item["name"].string_value() ;
        vector<PERSON_ID> kinships ;
        vector<float> feature ;
        for_each(item["kinships"].array_items().begin(), item["kinships"].array_items().end(), [&kinships](json11::Json item){kinships.push_back(item.string_value());}) ;
        for_each(item["feature"].array_items().begin(),item["feature"].array_items().end(),[&feature](json11::Json item){feature.push_back(item.number_value());} ) ;
        people[name] = Person(name, kinships, feature) ;
    } ;
    for_each(json.array_items().begin(), json.array_items().end(), parseItem) ;
}

void Database::loadDistance(string cache_file){
    jsonIO dis_cache = jsonIO(DISTANCE_CACHE) ;
    json11::Json json = dis_cache.readJson() ;
    /*auto parseItem = [this](json11::Json item){
        item.object_items()[0].first ;
    };*/
}

void Database::initDistance(function<float(vector<float>, Matrix)> metric, string method){
    Matrix m ;
    for(auto it=people.begin(); it!=people.end(); it++){
        for_each(people.begin(),people.end(),[this, m, metric, it](pair<PERSON_ID, Person> p){
            if(it->first==p.first){
                distanceBetween[it->first].push_back(pair<PERSON_ID, DISTANCE>(p.first,0.f)) ;
            }else{
                Distance d(it->second.feature, p.second.feature, m) ;
                distanceBetween[it->first].push_back(pair<PERSON_ID, DISTANCE>(p.first,d(metric))) ;
            }
        }) ;
        sort(distanceBetween.at(it->first).begin(), distanceBetween.at(it->first).end(),[](const pair<PERSON_ID,DISTANCE> &lhs, const pair<PERSON_ID,DISTANCE> &rhs)->bool{return rhs.second>lhs.second;}) ;
    }
    jsonIO json("metric_"+method+".json") ;
    json11::Json::array json_objects ;
    for(auto it=distanceBetween.begin(); it!=distanceBetween.end(); it++){
        json11::Json::object obj ;
        for_each(it->second.begin(), it->second.end(),[&obj](pair<PERSON_ID, DISTANCE> pd){obj[pd.first] = pd.second ;}) ;
        json_objects.push_back(obj) ;
    }
    json.writeJson(json_objects) ;
}

vector<Result> Database::query(PERSON_ID name) const{
    vector<Result> result ;
    for_each(distanceBetween.at(name).begin(), distanceBetween.at(name).end(),[&result](pair<PERSON_ID,DISTANCE> pd){result.push_back(Result(pd.first,pd.second));}) ;
             
    return result ;
}

QueryReport Database::queryReport(PERSON_ID name)const{
    vector<PERSON_ID> answer = people.at(name).kinships ;
    vector<int> queried_seq ;
    vector<float> percision, recall ;
    int i=1, found=0, total=answer.size() ;

    for(auto it = distanceBetween.at(name).begin(); it!=distanceBetween.at(name).end();it++, i++){
        auto f = find_if(answer.begin(),answer.end(), [it](PERSON_ID p)->bool{return it->first==p;}) ;
        if(f!=answer.end()){
            queried_seq.push_back(i+1) ;
            answer.erase(f) ;
            found++ ;
            //queryLog(name, it->first, i, it->second, true) ;
            queryLogger << "Query for " << name << ":rank " << i << ": " << it->first << ", distance:" << it->second  << "Hit"<<endl ;
        }
        else queryLogger << "Query for " << name << ":rank " << i << ": " << it->first << ", distance:" << it->second  << "Miss"<<endl ;
        percision.push_back((float)found/(float)i) ;
        recall.push_back((float)found/(float)total) ;
        if(answer.empty()) break ;
    }
    float percision_all = 0.f ;
    for_each(percision.begin(),percision.end(),[&percision_all](float p){percision_all+=p;}) ;
    float average_percision = percision_all / percision.size() ;
    return QueryReport(name, percision, recall, average_percision) ;
}

vector<QueryReport> Database::queryAllReport() const{
    vector<QueryReport> all_report ;
    for_each(people.begin(), people.end(), [&all_report, this](pair<PERSON_ID, Person> p){all_report.push_back(queryReport(p.first));}) ;
    return all_report ;
}

ostream& operator<<(ostream& out, const QueryReport& report) {
    out << "average percision for " << report.name<< ":" << report.averagePercision<< "queried at" << report.percision.size() << endl ;
    return out;
}