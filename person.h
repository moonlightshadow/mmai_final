//
//  person.h
//  MMAI_whoisyourdaddy
//
//  Created by O_o on 2015/12/15.
//  Copyright (c) 2015年 O_o. All rights reserved.
//

#ifndef __MMAI_whoisyourdaddy__person__
#define __MMAI_whoisyourdaddy__person__

#include <stdio.h>
#include <string>
#include <vector>
#define PERSON_ID string
#define DISTANCE float
using namespace std ;

struct Person{
    Person(){}
    Person(PERSON_ID name, vector<PERSON_ID> kinships, vector<float> feature):name(name), kinships(kinships), feature(feature){}
    PERSON_ID name ;
    vector<PERSON_ID> kinships ;
    vector<float> feature ;
    static string path ;
};

struct Result{
    Result(){} ;
    Result(PERSON_ID name, DISTANCE distance):name(name),distance(distance){}
    PERSON_ID name ;
    DISTANCE distance ;
    //bool operator >(const Result &b) const{return distance>b.distance;}
    
};





#endif /* defined(__MMAI_whoisyourdaddy__person__) */
