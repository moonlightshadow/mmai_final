//
//  classifier.h
//  MMAI_whoisyourdaddy
//
//  Created by O_o on 2015/12/15.
//  Copyright (c) 2015年 O_o. All rights reserved.
//

#ifndef __MMAI_whoisyourdaddy__classifier__
#define __MMAI_whoisyourdaddy__classifier__

#include <stdio.h>
#include "distance.h"
struct Classifier{
    Distance distance ;
    float margin ;
    unsigned int k ;
    function<float(vector<float>, Matrix)> lambda ;
    Classifier(Distance distance, float margin, unsigned int k, function<float(vector<float>, Matrix)> lambda):distance(distance),margin(margin),k(k),lambda(lambda) {}
    bool operator=(unsigned int rank)const{return distance(lambda)<margin && rank<=k ;}
};
#endif /* defined(__MMAI_whoisyourdaddy__classifier__) */
