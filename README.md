# MMAI 不滴血認親 #

嘗試以不滴血不驗DNA的方式，辨認兩張照片是否為親子(父子、父女、母子、母女)關係

Last update:1/4

### 進度/Changelog ###
* 1/4 21:00： LMNN SO FUCKING SLOW，Adaboost 60%
* 1/3 21:00： 新增L2 local area實驗結果，lmnn和adaboost WIP
* 1/1 18:00： 修改Evaluation。將Parent-Parent和Child-Child pairs移出negative set
* 12/30 22:00：修改Evaluation。原本Evaluation是100 positive和9900 negative，negative判斷權重太高，改成取Positive和Negative平均
* git branch很亂，很醜
* code很亂，很醜，lots of lambda spamming
* HOG+LBP L2 done

### 開發環境 ###

* MACOS 10.10 Xcode, clang(好像)
* C++11
* using json11 API for json parsing and writing

### Dataset ###

* http://www.kinfacew.com
目前使用父子、父女、母子、母女各100對


### 實驗結果4:naive l2 windows ###
* POSITIVE ACC:70% NEGATIVE ACC:52.1% MEAN:61.05%
* POSITIVE ACC:67% NEGATIVE ACC:60.7% MEAN:63.85%
* POSITIVE ACC:71% NEGATIVE ACC:51.475% MEAN:61.2375%
* POSITIVE ACC:63% NEGATIVE ACC:53.1% MEAN:58.05%
### 實驗結果3:Adaboost L2 local area###

* POSITIVE ACC:62.6263% NEGATIVE ACC:49.8458% MEAN:56.236%
* POSITIVE ACC:57.5758% NEGATIVE ACC:61.3477% MEAN:59.4617%
* POSITIVE ACC:56.5657% NEGATIVE ACC:51.743% MEAN:54.1543%
* POSITIVE ACC:58.5859% NEGATIVE ACC:52.441% MEAN:55.5134%


### 實驗結果2:naive L2 local area###

* Feature使用HOG+LBP local area, no reducing, ，共有64個area，維度分別為59和45
* Distance使用L2 distance
* Learning時，Margin取ground truth中Distance的平均
* 取Margin/Distance為Similarity排序
* 5 rounds，F/D, F/S, M/D, M/s
* MAP:0.356031 POSITIVE ACC:71.7172% NEGATIVE ACC:46.2254% MEAN:58.9713%
* MAP:0.460649 POSITIVE ACC:72.7273% NEGATIVE ACC:56.1712% MEAN:64.4492%
* MAP:0.419999 POSITIVE ACC:67.6768% NEGATIVE ACC:45.7194% MEAN:56.6981%
* MAP:0.41361 POSITIVE ACC:68.6869% NEGATIVE ACC:47.2823% MEAN:57.9846%
### 實驗結果1:naive L2 ###

* Feature使用HOG+LBP, no reducing, 維度分別為2880和3776
* Distance使用L2 distance
* Learning時，Margin取ground truth中Distance的平均
* 取Margin/Distance為Similarity排序
* 5 rounds，F/D, F/S, M/D, M/s
* MAP:0.37022 POSITIVE ACC:26.2626% NEGATIVE ACC:82.8708% MEAN:54.5667%
* MAP:0.478059 POSITIVE ACC:35.3535% NEGATIVE ACC:79.1678% MEAN:57.2607%
* MAP:0.428019 POSITIVE ACC:30.303% NEGATIVE ACC:74.563% MEAN:52.433%
* MAP:0.410112 POSITIVE ACC:44.4444% NEGATIVE ACC:73.1335% MEAN:58.789%
*
### some old data ###
* F/D Positive ACC 26.26%, Negative Acc 82.54%, Mean Acc 54.40%, MAP 19.17%
* F/S Positive ACC 35.35%, Negative Acc 90.95%, Mean Acc 60.65%, MAP 35.38%
* M/D Positive ACC 30.30%, Negative Acc 86.69%, Mean Acc 58.50%, MAP 31.45%
* M/S Positive ACC 44.44%, Negative Acc 80.58%, Mean Acc 62.51%, MAP 28.98%